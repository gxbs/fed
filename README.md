<div align="center">
  
  # My Fedora Workstation 🎋
  <sup2> Here you can find my Fedora installation and a **post-install-configuration** script </sup2>
  </div>
  
  > This scripts are made for the latest Fedora Workstation release (37 soon 38)
### Credits
Theme: <a href="https://github.com/lassekongo83/adw-gtk3" target="_blank" rel="noopener">**Adw-gtk3**</a>
<br> Local ISO Link: <a href="https://mirror.ihost.md/" target="_blank" rel="noopener">**iHost.md**</a>
<br>Apps: Discord, Viber, IntelliJ Idea and BalenaEtcher</br>
## Get the iso from:
* ISO page: [/workstation/download/](https://getfedora.org/en/workstation/download/)
* ISO page (beta): [/workstation/](https://stg.fedoraproject.org/workstation/)
* ISO itself: [**/x86_64/live/.iso**](https://download.fedoraproject.org/pub/fedora/linux/releases/37/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-37-1.7.iso)
* Local ISO (Moldova): [/releases/37/Workstation/x86_64/.iso](https://mirror.ihost.md/fedora/releases/37/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-37-1.7.iso)
## BalenaEtcher 
* Linux: [**Download**](https://github.com/balena-io/etcher/releases/download/v1.18.3/balenaEtcher-1.18.3-x64.AppImage)
* Windows: [Download](https://github.com/balena-io/etcher/releases/download/v1.18.3/balenaEtcher-Setup-1.18.3.exe)
* Mac: [Download](https://github.com/balena-io/etcher/releases/download/v1.18.3/balenaEtcher-1.18.3.dmg)
## Install
```
git clone https://github.com/GabsEdits/fed/
```
```
cd fed
```
```
sudo sh first.sh
```
```
sudo sh second.sh
```

#### Other scripts:
To add flathub repo **(going to be removed when f38 comes out)**: 
```
sudo sh ./flat.sh 
```
To add the *adw-gtk3* theme:
```
sudo sh ./theme.sh
```
<div align="center">
  <sup> Thanks for checking out my project 👋</sup>
